#include "persona.h"
#include <iostream>

using namespace std;

int main(int argc, char * const argv[])
{
Persona persona;
    persona.ingresar();
    persona.mostrar();
    Persona p1(20405041, "Perez", "Miguel");//definiendo constructor sobrecargado
    cout << persona.toString() <<" - " << p1.toString () << endl;
    return 0;
}
