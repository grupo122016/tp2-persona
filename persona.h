#include <iostream>
#include <string>
#ifndef PERSONA_H
#define PERSONA_H

using namespace std;

class Persona //definiendo la clase
{
    public:
        Persona(); //constructor
        Persona (int,string,string);

        void ingresar();
        void mostrar();

        void setDni(int);
        int getDni();
        void setNombre(string);
        string getNombre();
        void setApellido(string);
        string getApellido();
        string toString();
    protected:
    private:
        int dni;
        string nombre;
        string apellido;
};

#endif // PERSONA_H
